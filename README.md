# TP 2 - Machine virtuelle, réseau, serveurs, routage simple
   ### 4. Configuration réseau d'une machine CentOS
   Liste des cartes réseau
   | Name   | IP        | MAC               | Fonction                |
   |--------|-----------|-------------------|-------------------------|
   | lo     | 127.0.0.1 | 00:00:00:00:00:00 | Carte de loopback       |
   | enp0s3 | 10.0.2.15 | 08:00:27:db:61:76 | Carte réseau NAT        |
   | enp0s8 | 10.2.1.2  | 08:00:27:86:f1:fb | Carte réseau privé hôte |
   On va changer l'IP de la carte réseau hôte
   
   IP initiale : 10.2.1.4
   ```bash
   [root@localhost ~]# ip a
   1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
      link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
      inet 127.0.0.1/8 scope host lo
         valid_lft forever preferred_lft forever
      inet6 ::1/128 scope host
         valid_lft forever preferred_lft forever
   2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
      link/ether 08:00:27:db:61:76 brd ff:ff:ff:ff:ff:ff
      inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
         valid_lft 86101sec preferred_lft 86101sec
      inet6 fe80::f39d:8127:1f12:abfd/64 scope link noprefixroute
         valid_lft forever preferred_lft forever
   3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
      link/ether 08:00:27:86:f1:fb brd ff:ff:ff:ff:ff:ff
      inet 10.2.1.4/24 brd 10.2.1.255 scope global noprefixroute enp0s8
         valid_lft forever preferred_lft forever
      inet6 fe80::c87c:9447:9db2:38f9/64 scope link noprefixroute
         valid_lft forever preferred_lft forever
   ```
                                                                                                                                          
   
   Pour changer l'IP, on se rend dans `cd /etc/sysconfig/network-scripts`
   Pour changer l'IP de la carte réseau hôte, on va modifier via vim le fichier `ifcfg-enp0s8` et changer l'adresse à la ligne `IPADDR="10.2.1.4"`. On va la changer en `IPADDR="10.2.1.5"`.
   
   On fait un  `service network restart` pour redémarrer le service réseau et boum l'IP est changée
   ```bash
   3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
      link/ether 08:00:27:86:f1:fb brd ff:ff:ff:ff:ff:ff
      inet 10.2.1.5/24 brd 10.2.1.255 scope global noprefixroute enp0s8
         valid_lft forever preferred_lft forever
      inet6 fe80::c87c:9447:9db2:38f9/64 scope link noprefixroute
         valid_lft forever preferred_lft forever
   ```
   On test un ping
   ```powershell
   PS C:\Windows\system32> ping 10.2.1.5
   
   Envoi d’une requête 'Ping'  10.2.1.5 avec 32 octets de données :
   Réponse de 10.2.1.5 : octets=32 temps<1ms TTL=64
   Réponse de 10.2.1.5 : octets=32 temps=1 ms TTL=64
   Réponse de 10.2.1.5 : octets=32 temps<1ms TTL=64
   Réponse de 10.2.1.5 : octets=32 temps<1ms TTL=64
   
   Statistiques Ping pour 10.2.1.5:
      Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
   Durée approximative des boucles en millisecondes :
      Minimum = 0ms, Maximum = 1ms, Moyenne = 0ms
   ```
   ### 5. Appréhension de quelques commandes
   On lance un scan 
   ```bash
   [root@localhost ~]#  nmap -A 10.2.1.5
   
   Starting Nmap 6.40 ( http://nmap.org ) at 2020-02-13 15:40 CET
   Nmap scan report for 10.2.1.5
   Host is up (0.00015s latency).
   Not shown: 999 closed ports
   PORT   STATE SERVICE VERSION
   22/tcp open  ssh     OpenSSH 7.4 (protocol 2.0)
   | ssh-hostkey: 2048 d6:21:5e:21:1e:ac:14:bd:0f:b7:2e:0a:1e:c8:8c:a9 (RSA)
   |_256 8b:a3:1d:26:60:10:ab:e1:6e:9b:2e:bb:67:e7:66:43 (ECDSA)
   Device type: general purpose
   Running: Linux 3.X
   OS CPE: cpe:/o:linux:linux_kernel:3
   OS details: Linux 3.7 - 3.9
   Network Distance: 0 hops
   
   OS and Service detection performed. Please report any incorrect results at http://nmap.org/submit/ .
   Nmap done: 1 IP address (1 host up) scanned in 2.97 seconds
   ```
   On scanne les ports TCP/UDP en écoute
   ```powershell
   [root@localhost ~]# ss -ltunp
   Netid  State      Recv-Q Send-Q                              
   udp    UNCONN     0      0                                                *:68                                                           *:*                   users:(("dhclient",pid=3193,fd=6))
   udp    UNCONN     0      0                                        127.0.0.1:323                                                          *:*                   users:(("chronyd",pid=795,fd=5))
   udp    UNCONN     0      0                                            [::1]:323                                                       [::]:*                   users:(("chronyd",pid=795,fd=6))
   tcp    LISTEN     0      100                                      127.0.0.1:25                                                           *:*                   users:(("master",pid=1540,fd=13))
   tcp    LISTEN     0      128                                              *:22                                                           *:*                   users:(("sshd",pid=1215,fd=3))
   tcp    LISTEN     0      100                                          [::1]:25                                                        [::]:*                   users:(("master",pid=1540,fd=14))
   tcp    LISTEN     0      128                                           [::]:22                                                        [::]:*                   users:(("sshd",pid=1215,fd=4))
   ```
   On peut check quel programme envoie/reçoit des données grâce au PID ou au nom juste en face.
   Par exemple, le programme `sshd` qui envoie des données doit être le daemon de la VM.
   ## II. Notion de ports
   ### 1. SSH
   On peut check sur quel port le serveur SSH écoute avec la commande utilisée précédemment. On peut voir que `sshd` écoute sur le port 22.
   
   On peut check la connexion ssh du port 22 avec la commande `ss -l -t`
   ```powershell
   [root@localhost ~]# ss -l -t
   State      Recv-Q Send-Q                                Local Address:Port                                                 Peer Address:Port
   LISTEN     0      100                                       127.0.0.1:smtp                                                            *:*
   LISTEN     0      128                                               *:ssh                                                             *:*
   LISTEN     0      100                                           [::1]:smtp                                                         [::]:*
   LISTEN     0      128                                            [::]:ssh                                                          [::]:*
   
   ```
   On peut voir la ligne `ssh` présente.
   Pour modifier le port du service SSH, il faut se rendre dans `/etc/ssh` et modifier le fichier `sshd_config`
   
   A la ligne `# Port 22`, on enlève le # et on met le port souhaité, (moi j'ai mis 2222) on sauvegarde, on redémarre le service SSH via `systemctl` et on observe que ça marche pas yes. La firewall bloque le nouveau port du SSH, CentOS a un firewall de base. Du coup on va ouvrir le port correspondant du firewall.
   
   Pour se faire, on va tout simplement taper la commande `sudo firewall-cmd --add-port=2222/tcp --permanent` pour ajouter le port 2222 aux ports autorisés par le firewall. Puis on recharge le tout avec la commande `sudo firewall-cmd --reload`.
   Boum, le port est ouvert
   ```powershell
   [root@localhost ~]# sudo firewall-cmd --list-all
   public (active)
   target: default
   icmp-block-inversion: no
   interfaces: enp0s3 enp0s8
   sources:
   services: dhcpv6-client ssh
   ports: 2222/tcp
   protocols:
   masquerade: no
   forward-ports:
   source-ports:
   icmp-blocks:
   rich rules:
   ```
   
   ### B. Netcat
   
   On fait ce qui est demandé de faire et on lance 2 powershell sur le PC.
   On connecte l'un des powershell à la VM avec `nc -L -p 1025` et sur la VM, on exécute la commande `nc 192.168.0.20 1025` pour qu'ils puissent communiquer.
   Sur le second powershell, on utilise netstat pour voir les connections entrantes 
   ```powershell
   PS C:\Windows\System32\WindowsPowerShell\v1.0> netstat
   
   Connexions actives
   
   Proto  Adresse locale         Adresse distante       État
   TCP    10.2.1.1:19022         10.2.1.5:ssh           ESTABLISHED
   [...]
   ```
   
 
 ## III. Routage statique

### 2. Configuration du routage
#### A. PC1

Ping de PC2(10.2.2.1) depuis PC1:

```bash
C:\Windows\system32>ping 10.2.2.1

Envoi d’une requête 'Ping'  10.2.2.1 avec 32 octets de données :
Réponse de 10.2.2.1 : octets=32 temps=3 ms TTL=127
Réponse de 10.2.2.1 : octets=32 temps=1 ms TTL=127
Réponse de 10.2.2.1 : octets=32 temps=1 ms TTL=127
Réponse de 10.2.2.1 : octets=32 temps=2 ms TTL=127

Statistiques Ping pour 10.2.2.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 3ms, Moyenne = 1ms
```
#### B. PC2

Ping de PC1(10.2.1.1) depuis PC2 :

```bash
C:\WINDOWS\system32>ping 10.2.1.1

Envoi d’une requête 'Ping'  10.2.1.1 avec 32 octets de données :
Réponse de 10.2.1.1 : octets=32 temps=1 ms TTL=127
Réponse de 10.2.1.1 : octets=32 temps=2 ms TTL=127

Statistiques Ping pour 10.2.1.1:
    Paquets : envoyés = 2, reçus = 2, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms
```
#### C. VM1
Commande utiliser pour ajouter une route :
```bash
ip route add 10.2.2.0/24 via 10.2.1.1 dev enp0s8
```

Ping PC2(10.2.2.1) depuis VM1 :
```bash
[centos@localhost ~]$ ping 10.2.2.1
PING 10.2.2.1 (10.2.2.1) 56(84) bytes of data.
64 bytes from 10.2.2.1: icmp_seq=1 ttl=126 time=1.88 ms
64 bytes from 10.2.2.1: icmp_seq=2 ttl=126 time=1.98 ms
64 bytes from 10.2.2.1: icmp_seq=3 ttl=126 time=2.78 ms
^C
--- 10.2.2.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2018ms
rtt min/avg/max/mdev = 1.889/2.222/2.789/0.404 ms
```
#### D. VM2

Ping PC1 depuis VM2:
```bash
[centos@localhost ~]$ ping 10.2.1.1
PING 10.2.1.1 (10.2.1.1) 56(84) bytes of data.
64 bytes from 10.2.1.1: icmp_seq=1 ttl=126 time=2.01 ms
64 bytes from 10.2.1.1: icmp_seq=2 ttl=126 time=2.56 ms
64 bytes from 10.2.1.1: icmp_seq=3 ttl=126 time=3.22 ms
64 bytes from 10.2.1.1: icmp_seq=4 ttl=126 time=2.84 ms
64 bytes from 10.2.1.1: icmp_seq=5 ttl=126 time=2.57 ms
^C
--- 10.2.1.1 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4009ms
rtt min/avg/max/mdev = 2.015/2.644/3.229/0.398 ms
```

#### E. El gran final

Ping VM1(10.2.1.2) depuis VM2:
```bash
[centos@localhost ~]$ ping 10.2.1.2
PING 10.2.1.2 (10.2.1.2) 56(84) bytes of data.
64 bytes from 10.2.1.2: icmp_seq=1 ttl=62 time=1.90 ms
64 bytes from 10.2.1.2: icmp_seq=2 ttl=62 time=2.37 ms
64 bytes from 10.2.1.2: icmp_seq=3 ttl=62 time=2.78 ms
64 bytes from 10.2.1.2: icmp_seq=4 ttl=62 time=3.02 ms
64 bytes from 10.2.1.2: icmp_seq=5 ttl=62 time=2.26 ms
^C
--- 10.2.1.2 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4011ms
rtt min/avg/max/mdev = 1.906/2.469/3.023/0.397 ms
```

Ping VM2 depuis VM1:
```bash
[centos@localhost ~]$ ping 10.2.2.2
PING 10.2.2.2 (10.2.2.2) 56(84) bytes of data.
64 bytes from 10.2.2.2: icmp_seq=1 ttl=62 time=1.85 ms
64 bytes from 10.2.2.2: icmp_seq=2 ttl=62 time=3.16 ms
64 bytes from 10.2.2.2: icmp_seq=3 ttl=62 time=3.54 ms
^C
--- 10.2.2.2 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2007ms
rtt min/avg/max/mdev = 1.853/2.855/3.544/0.724 ms
```

### 3. Configuration des noms de domaine

VM1 ping VM2 avec nom dommaine:
```bash
[centos@localhost ~]$ ping vm2.tp2.b1
PING vm2.tp2.b1 (10.2.2.2) 56(84) bytes of data.
64 bytes from vm2.tp2.b1 (10.2.2.2): icmp_seq=1 ttl=62 time=3.89 ms
64 bytes from vm2.tp2.b1 (10.2.2.2): icmp_seq=2 ttl=62 time=3.01 ms
64 bytes from vm2.tp2.b1 (10.2.2.2): icmp_seq=3 ttl=62 time=2.47 ms
^C
--- vm2.tp2.b1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 2.477/3.127/3.894/0.584 ms
```

Traceroute de la VM2 depuis VM1 :
```bash
[centos@localhost ~]$ traceroute vm2.tp2.b1
traceroute to vm2.tp2.b1 (10.2.2.2), 30 hops max, 60 byte packets
 1  gateway (10.0.2.2)  0.191 ms  0.101 ms  0.070 ms
 2  gateway (10.0.2.2)  2.520 ms  2.408 ms  2.307 ms

